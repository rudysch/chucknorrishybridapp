import { Component } from '@angular/core';
import {AlertController} from '@ionic/angular';
import {Observable} from 'rxjs';
import {AnecdoteService} from '../service/anecdote.service';
import {resolve} from '@angular-devkit/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
    anecdotes: Observable<any>;
    constructor(private alertController: AlertController, private anecdoteService: AnecdoteService) {
        this.anecdotes = this.anecdoteService.getRandom();
        this.anecdotes
            .subscribe(data => {
                this.anecdotes=data;
                console.log('my data: ', data);
            });
    }
}