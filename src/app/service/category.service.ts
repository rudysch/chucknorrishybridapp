import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class CategoryService {
    private BASE_URL = 'https://api.chucknorris.io/';

    constructor(
        protected httpClient: HttpClient,
    ) {}
    getAllCategories() {
        return this.httpClient.get<any>(`${this.BASE_URL}jokes/categories`);
    }
    getOneQuoteFromSpecificCategory(categoryString){
        return this.httpClient.get<any>(`${this.BASE_URL}jokes/random?category=${categoryString}`);
    }
}
