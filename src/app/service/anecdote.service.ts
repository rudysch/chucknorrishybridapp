import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class AnecdoteService {
    private BASE_URL = 'https://api.chucknorris.io/';

    constructor(
        protected httpClient: HttpClient,
    ) {}
    getRandom() {
        return this.httpClient.get<any>(`${this.BASE_URL}jokes/random`);
    }
    get(id) {
        return this.httpClient.get<any>(`${this.BASE_URL}/${id}`);
    }
}
