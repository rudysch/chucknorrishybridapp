import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryService } from '../service/category.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {

  categories: Observable<any>
  quoteFromCategoryString : Observable<any>;

  constructor(public alertController: AlertController, private categoryService: CategoryService) {
    this.categories = this.categoryService.getAllCategories();
    this.categories
        .subscribe(data => {
          this.categories=data;
          console.log('my data: ', data);
        })
   };
  
  async openRandomQuote(stringCategory){
      this.quoteFromCategoryString = this.categoryService.getOneQuoteFromSpecificCategory(stringCategory);
      this.quoteFromCategoryString
        .subscribe(data => {
          this.quoteFromCategoryString=data.value;
          console.log('my randomQuoteFromCategory: ', data);
        })
      const alert = await this.alertController.create({
        header: 'Alert',
        subHeader: 'Subtitle',
        message: this.quoteFromCategoryString,
        buttons: ['OK']
      });
  
      await alert.present();
    }

  ngOnInit() {
  }

}
